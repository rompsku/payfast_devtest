<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Theatre extends Model
{

    public const STATUS_SETTINGS = [
        self::STATUS_INACTIVE,
        self::STATUS_ACTIVE
    ];

    public const STATUS_INACTIVE = 0; // 
    public const STATUS_ACTIVE = 1; // 


    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'cinema_id',
        'status',
        'title',
        'description',
        'image_seating_grid',
        'image_thumb',
        'image_large',
        'image_interior_1',
        'image_interior_2',
        'image_interior_3'
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    public function seats()
    {
        return $this->hasMany(Seat::class);
    }

    public function cinema()
    {
        return $this->belongsTo(Cinema::class);
    }
}
