<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Session extends Model
{
    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    public const STATUS_SETTINGS = [
        self::STATUS_INACTIVE,
        self::STATUS_ACTIVE
    ];

    public const STATUS_INACTIVE = 0; // 
    public const STATUS_ACTIVE = 1; // 

    /**
     * @var array
     */
    protected $fillable = [
        'session_token',
        'ip',
        'user_id',
        'status',
        'expires_at'
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;


    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
}
