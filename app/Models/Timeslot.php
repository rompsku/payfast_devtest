<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Timeslot extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'theatre_id',
        'movie_id',
        'reserved_seating',
        'starts_at',
    ];


    /**
     * The attributes that should be cast.
     *
     * @var array <int, string>
     */
    protected $casts = [
        'starts_at' => 'datetime',

    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    protected function getStartsAtAttribute($value)
    {
        return Carbon::parse($value)->isoFormat('dddd, Do MMMM @ h:mm a');
    }


    public function bookings()
    {
        return $this->hasMany(Booking::class)->with('tickets');
    }

    public function tickets()
    {
        return $this->hasManyThrough(Ticket::class, Booking::class);
    }

    public function movie()
    {
        return $this->belongsTo(Movie::class);
    }

    public function theatre()
    {
        return $this->belongsTo(Theatre::class)->with('cinema');
    }

    public function seats()
    {
        return $this->hasManyThrough(Seat::class, Theatre::class);
    }
}
