<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'user_id',
        'booking_reference',
        'timeslot_id',
        'client_notes'
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;
    
    public function timeslot()
    {
        return $this->belongsTo(Timeslot::class)->with(['theatre', 'movie']);
    }

    public function tickets()
    {
        return $this->hasMany(Ticket::class)->with('seat');
    }

    public function seats()
    {
        return $this->hasManyThrough(Seat::class, Ticket::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
