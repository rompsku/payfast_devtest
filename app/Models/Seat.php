<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Seat extends Model
{

    public const STATUS_SETTINGS = [
        self::STATUS_INACTIVE,
        self::STATUS_ACTIVE
    ];

    public const STATUS_INACTIVE = 0; // 
    public const STATUS_ACTIVE = 1; // 

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'theatre_id',
        'status',
        'location_x',
        'location_y'
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    public function theater()
    {
        return $this->belongsTo(Theatre::class);
    }

    public function tickets()
    {
        return $this->hasMany(Ticket::class);
    }
}
