<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Model
{

    use SoftDeletes;

    public const STATUS_SETTINGS = [
        self::STATUS_INACTIVE,
        self::STATUS_ACTIVE
    ];

    public const STATUS_INACTIVE = 0; // 
    public const STATUS_ACTIVE = 1; // 

    /**
     * The attributes that are mass assignable.
     *
     * @var array <int, string>
     */
    protected $fillable = [
        'role_id',
        'status',
        'name',
        'email',
        'password',
        'password_reset_expires_at',
        'password_reset_key'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array <int, string>
     */
    protected $hidden = [
        'password',
        'password_reset_expires_at',
        'password_reset_key'

    ];

    /**
     * The attributes that should be cast.
     *
     * @var array <string, string>
     */
    protected $casts = [
        'password_reset_expires_at' => 'datetime',
    ];


    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;
    
    public function role()
    {
        return $this->hasOne(Role::class);
    }

    public function bookings()
    {
        return $this->hasMany(Booking::class);
    }

    public function tickets()
    {
        return $this->hasManyThrough(Ticket::class, Booking::class);
    }
}
