<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Movie extends Model
{
    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    public const STATUS_SETTINGS = [
        self::STATUS_INACTIVE,
        self::STATUS_ACTIVE
    ];

    public const STATUS_INACTIVE = 0; // 
    public const STATUS_ACTIVE = 1; // 


    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'title',
        'status',
        'age_rating',
        'runtime',
        'plot',
        'genres',
        'director',
        'writer',
        'actors',
        'other_actors',
        'languages',
        'countries',
        'awards',
        'poster',
        'trailer',
        'imdb_rating',
        'imdb_id',
        'box_office',
        'release_date'
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'release_date' => 'date'
    ];


    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    public function timeslots()
    {
        return $this->hasMany(Timeslot::class)->with('theatre');
    }

    public function bookings()
    {
        return $this->hasManyThrough(Booking::class, Timeslot::class)->with('tickets');
    }
}
