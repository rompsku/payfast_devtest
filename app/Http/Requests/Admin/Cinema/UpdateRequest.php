<?php

namespace App\Http\Requests\Admin\Cinema;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'title' => 'required|string|unique:cinemas,title,' . $this->route('id'),
            'status' => 'required|boolean',
            'geo_location' => 'nullable|string',
            'address' => 'nullable|string',
            'description' => 'nullable|string',
            'image_thumb' => 'nullable|URL',
            'image_large' => 'nullable|URL',
            'image_interior' => 'nullable|URL',
            'image_exterior' => 'nullable|URL',
            'preview_clip' => 'nullable|URL'
        ];
    }
}
