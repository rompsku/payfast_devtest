<?php

namespace App\Http\Requests\Admin\User;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'role_id' => 'required|integer|min:1|max:2',
            'status' => 'required|boolean',
            'name' => 'required|regex:/^[a-z_\ \-\d]{4,}$/i|unique:users,name,' . $this->route('id'),
            'email' => 'required|email|unique:users,email,' . $this->route('id'),
            'password' => 'required|min:6',
        ];
    }

    public function messages(): array
    {
        return [
            'name.regex' => 'Name must be longer than 4 characters and may only contain letters, numbers and underscores.',
        ];
    }
}
