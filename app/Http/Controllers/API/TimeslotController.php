<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\{Response, JsonResponse};
use DB;
use App\Models\{Timeslot};
use Illuminate\Database\Eloquent\Builder;

class TimeslotController extends Controller
{
    /**
     * Retreive list of filtered movie objects
     *
     * @return JsonResponse
     */
    function index(): JsonResponse
    {
        if (!request()->get('cinema_id') || !request()->has('movie_id')) {
            return response()->json(
                [
                    "status" => "Error",
                    "message" => "Invalid request"
                ],
                Response::HTTP_INTERNAL_SERVER_ERROR,
            );
        }

        /**
         * Initial builder instance.
         */
        $query = Timeslot::query()->with('theatre')
            ->where('starts_at', '>', now()->addHours(1))
            ->where('starts_at', '<', now()->addWeeks(2))
            ->where('movie_id', request()->get('movie_id'))
            ->whereHas('theatre', function (Builder $sub_query) {
                $sub_query->where('cinema_id', request()->get('cinema_id'));
            });

        return response()->json(
            [
                "status" => "Success",
                "message" => "Data retrieved successfully",
                "data" => [
                    "timeslots" => $query->orderBy('starts_at', 'ASC')->get(),
                    "count" => $query->count()
                ]
            ],
            Response::HTTP_OK,
        );
    }
}
