<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\{Response, JsonResponse};
use DB;
use App\Models\{Cinema};

class CinemaController extends Controller
{
    /**
     * Retreive list of filtered cinema objects
     *
     * @return JsonResponse
     */
    function index(): JsonResponse
    {

        /**
         * Initial builder instance.
         */
        $query = Cinema::query()->withCount(['theatres']);

        return response()->json(
            [
                "status" => "Success",
                "message" => "Data retrieved successfully",
                "data" => [
                    "cinemas" => $query->get(),
                    "count" => $query->count()
                ]
            ],
            Response::HTTP_OK,
        );
    }
}
