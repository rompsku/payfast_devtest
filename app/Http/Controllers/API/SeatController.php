<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\{Response, JsonResponse};
use DB;
use App\Models\{Timeslot, Seat};
use Illuminate\Database\Eloquent\Builder;

class SeatController extends Controller
{
    /**
     * Retreive list of filtered movie objects
     *
     * @return JsonResponse
     */
    function index(): JsonResponse
    {
        if (!request()->get('timeslot_id')) {
            return response()->json(
                [
                    "status" => "Error",
                    "message" => "Invalid request"
                ],
                Response::HTTP_INTERNAL_SERVER_ERROR,
            );
        }

        //
        $timeslot = Timeslot::whereId(request()->get('timeslot_id'))->firstorFail();
        /**
         * Initial builder instance.
         */
        $query = Seat::query()
            ->where('theatre_id', $timeslot->theatre_id)
            ->with('tickets', function ($sub_query) {
                $sub_query->whereHas('booking',  function ($ss_query) {
                    $ss_query->where('timeslot_id', request()->get('timeslot_id'));
                });
            });

        return response()->json(
            [
                "status" => "Success",
                "message" => "Data retrieved successfully",
                "data" => [
                    "seats" => $query->orderBy('location_x', 'ASC')->orderBy('location_y', 'ASC')->get(),
                    "count" => $query->count()
                ]
            ],
            Response::HTTP_OK,
        );
    }
}
