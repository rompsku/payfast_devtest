<?php

namespace App\Http\Controllers\API\Admin\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\{Response, JsonResponse};
use DB;
use App\Models\{User, Role};
use App\Http\Requests\Admin\User\{CreateRequest, UpdateRequest};

class CRUDController extends Controller
{
    /**
     * Retreive list of filtered user objects
     *
     * @return JsonResponse
     */
    function index(): JsonResponse
    {

        /**
         * Initial builder instance.
         */
        $query = User::query()->withCount(['bookings', 'tickets']);

        /**
         * The count of all users.
         */
        $count = $query->count();

        //
        $users = $query->get();

        return response()->json(
            [
                "status" => "Success",
                "message" => "Data retrieved successfully",
                "data" => [
                    "users" => $users,
                    "count" => $count
                ]
            ],
            Response::HTTP_OK,
        );
    }

    /**
     * Retrieve a single user object
     *
     * @return JsonResponse
     */
    function show($id): JsonResponse
    {
        /**
         * Initial builder instance.
         */
        $user = User::query()->with(['bookings', 'tickets'])
            ->whereId($id)->firstOrFail();

        return response()->json(
            [
                "status" => "Success",
                "message" => "Data retrieved successfully",
                "data" => [
                    "user" => $user
                ]
            ],
            Response::HTTP_OK,
        );
    }

    /**
     * Create new user object
     *
     * @param CreateRequest $request
     * @return JsonResponse
     */
    function create(CreateRequest $request): JsonResponse
    {
        return DB::transaction(function () use ($request) {
            
            // Create user object in database
            $user = User::create([
                'role_id' => $request->input('role_id'),
                'status' => $request->input('status'),
                'name' => $request->input('name'),
                'email' => $request->input('email'),
                'password' => md5($request->input('password')), // password
            ]);

            return response()->json(
                [
                    "status" => "Success",
                    "message" => "Data stored successfully",
                    "data" => [
                        "user" => $user,
                    ]
                ],
                Response::HTTP_OK
            );
        });
    }

    /**
     * Update a user object
     *
     * @param UpdateRequest $request
     * @return JsonResponse
     */
    function update(UpdateRequest $request, $id): JsonResponse
    {
        return DB::transaction(function () use ($request, $id) {

            // update user object in database
            User::whereId($id)->update([
                'role_id' => request('role_id'),
                'status' => request('status'),
                'name' => $request->input('name'),
                'email' => $request->input('email'),
                'password' => md5($request->input('password')), // password
            ]);

            return response()->json(
                [
                    "status" => "Success",
                    "message" => "Data updated successfully"
                ],
                Response::HTTP_OK
            );
        });
    }

    /**
     * Delete a user object
     *
     * @return JsonResponse
     */
    function delete($id): JsonResponse
    {
        return DB::transaction(function () use ($id) {

            // delete user object from DB    
            User::whereId($id)->delete();

            return response()->json(
                [
                    "status" => "Success",
                    "message" => "Data deleted successfully"
                ],
                Response::HTTP_OK
            );
        });
    }
}
