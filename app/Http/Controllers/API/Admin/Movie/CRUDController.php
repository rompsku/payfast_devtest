<?php

namespace App\Http\Controllers\API\Admin\Movie;

use App\Http\Controllers\Controller;
use Illuminate\Http\{Response, JsonResponse};
use DB;
use App\Models\{Movie};
use App\Http\Requests\Admin\Movie\{CreateRequest, UpdateRequest};

class CRUDController extends Controller
{
    /**
     * Retreive list of filtered movie objects
     *
     * @return JsonResponse
     */
    function index(): JsonResponse
    {

        /**
         * Initial builder instance.
         */
        $query = Movie::query()->withCount(['bookings']);

        /**
         * The count of all movies.
         */
        $count = $query->count();

        //
        $movies = $query->get();

        return response()->json(
            [
                "status" => "Success",
                "message" => "Data retrieved successfully",
                "data" => [
                    "movies" => $movies,
                    "count" => $count
                ]
            ],
            Response::HTTP_OK,
        );
    }

    /**
     * Retrieve a single movie object
     *
     * @return JsonResponse
     */
    function show($id): JsonResponse
    {
        /**
         * Initial builder instance.
         */
        $movie = Movie::query()->with(['timeslots'])
            ->whereId($id)->firstOrFail();

        return response()->json(
            [
                "status" => "Success",
                "message" => "Data retrieved successfully",
                "data" => [
                    "movie" => $movie
                ]
            ],
            Response::HTTP_OK,
        );
    }

    /**
     * Create new movie object
     *
     * @param CreateRequest $request
     * @return JsonResponse
     */
    function create(CreateRequest $request): JsonResponse
    {
        return DB::transaction(function () use ($request) {
            
            // Create movie object in database
            $movie = Movie::create([
                'title' => $request->input('title'),
                'status' => $request->input('status'),
                'geo_location' => $request->input('geo_location'),
                'address' => $request->input('address'),
                'description' => $request->input('description'),
                'image_thumb' => $request->input('image_thumb'),
                'image_large' => $request->input('image_large'),
                'image_interior' => $request->input('image_interior'),
                'image_exterior' => $request->input('image_exterior'),
                'preview_clip' => $request->input('preview_clip')
            ]);

            return response()->json(
                [
                    "status" => "Success",
                    "message" => "Data stored successfully",
                    "data" => [
                        "movie" => $movie,
                    ]
                ],
                Response::HTTP_OK
            );
        });
    }

    /**
     * Update a movie object
     *
     * @param UpdateRequest $request
     * @return JsonResponse
     */
    function update(UpdateRequest $request, $id): JsonResponse
    {
        return DB::transaction(function () use ($request, $id) {
            
            // update movie object in database
            Movie::whereId($id)->update([
                'title' => $request->input('title'),
                'status' => $request->input('status'),
                'geo_location' => $request->input('geo_location'),
                'address' => $request->input('address'),
                'description' => $request->input('description'),
                'image_thumb' => $request->input('image_thumb'),
                'image_large' => $request->input('image_large'),
                'image_interior' => $request->input('image_interior'),
                'image_exterior' => $request->input('image_exterior'),
                'preview_clip' => $request->input('preview_clip')
            ]);

            return response()->json(
                [
                    "status" => "Success",
                    "message" => "Data updated successfully"
                ],
                Response::HTTP_OK
            );
        });
    }

    /**
     * Delete a movie object
     *
     * @return JsonResponse
     */
    function delete($id): JsonResponse
    {
        return DB::transaction(function () use ($id) {

            // delete movie object from DB    
            Movie::whereId($id)->delete();

            return response()->json(
                [
                    "status" => "Success",
                    "message" => "Data deleted successfully"
                ],
                Response::HTTP_OK
            );
        });
    }
}
