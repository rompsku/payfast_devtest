<?php

namespace App\Http\Controllers\API\Admin\Cinema;

use App\Http\Controllers\Controller;
use Illuminate\Http\{Response, JsonResponse};
use DB;
use App\Models\{Cinema};
use App\Http\Requests\Admin\Cinema\{CreateRequest, UpdateRequest};

class CRUDController extends Controller
{
    /**
     * Retreive list of filtered cinema objects
     *
     * @return JsonResponse
     */
    function index(): JsonResponse
    {

        /**
         * Initial builder instance.
         */
        $query = Cinema::query()->withCount(['theatres']);

        /**
         * The count of all cinemas.
         */
        $count = $query->count();

        //
        $cinemas = $query->get();

        return response()->json(
            [
                "status" => "Success",
                "message" => "Data retrieved successfully",
                "data" => [
                    "cinemas" => $cinemas,
                    "count" => $count
                ]
            ],
            Response::HTTP_OK,
        );
    }

    /**
     * Retrieve a single cinema object
     *
     * @return JsonResponse
     */
    function show($id): JsonResponse
    {
        /**
         * Initial builder instance.
         */
        $cinema = Cinema::query()->with(['timeslots', 'theatres'])
            ->whereId($id)->firstOrFail();

        return response()->json(
            [
                "status" => "Success",
                "message" => "Data retrieved successfully",
                "data" => [
                    "cinema" => $cinema
                ]
            ],
            Response::HTTP_OK,
        );
    }

    /**
     * Create new cinema object
     *
     * @param CreateRequest $request
     * @return JsonResponse
     */
    function create(CreateRequest $request): JsonResponse
    {
        return DB::transaction(function () use ($request) {
            
            // Create cinema object in database
            $cinema = Cinema::create([
                'title' => $request->input('title'),
                'status' => $request->input('status'),
                'geo_location' => $request->input('geo_location'),
                'address' => $request->input('address'),
                'description' => $request->input('description'),
                'image_thumb' => $request->input('image_thumb'),
                'image_large' => $request->input('image_large'),
                'image_interior' => $request->input('image_interior'),
                'image_exterior' => $request->input('image_exterior'),
                'preview_clip' => $request->input('preview_clip')
            ]);

            return response()->json(
                [
                    "status" => "Success",
                    "message" => "Data stored successfully",
                    "data" => [
                        "cinema" => $cinema,
                    ]
                ],
                Response::HTTP_OK
            );
        });
    }

    /**
     * Update a cinema object
     *
     * @param UpdateRequest $request
     * @return JsonResponse
     */
    function update(UpdateRequest $request, $id): JsonResponse
    {
        return DB::transaction(function () use ($request, $id) {
            
            // update cinema object in database
            Cinema::whereId($id)->update([
                'title' => $request->input('title'),
                'status' => $request->input('status'),
                'geo_location' => $request->input('geo_location'),
                'address' => $request->input('address'),
                'description' => $request->input('description'),
                'image_thumb' => $request->input('image_thumb'),
                'image_large' => $request->input('image_large'),
                'image_interior' => $request->input('image_interior'),
                'image_exterior' => $request->input('image_exterior'),
                'preview_clip' => $request->input('preview_clip')
            ]);

            return response()->json(
                [
                    "status" => "Success",
                    "message" => "Data updated successfully"
                ],
                Response::HTTP_OK
            );
        });
    }

    /**
     * Delete a cinema object
     *
     * @return JsonResponse
     */
    function delete($id): JsonResponse
    {
        return DB::transaction(function () use ($id) {

            // delete cinema object from DB    
            Cinema::whereId($id)->delete();

            return response()->json(
                [
                    "status" => "Success",
                    "message" => "Data deleted successfully"
                ],
                Response::HTTP_OK
            );
        });
    }
}
