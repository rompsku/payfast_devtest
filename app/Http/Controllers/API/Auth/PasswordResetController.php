<?php

namespace App\Http\Controllers\API\Auth;


use App\Http\Controllers\Controller;
use Illuminate\Http\{Response, JsonResponse};
use Illuminate\Support\Facades\DB;
use App\Models\{User, Role, Session};
use App\Http\Requests\Auth\{PasswordResetRequest};

class PasswordResetController extends Controller
{
    /**
     * Password reset request
     *
     * @param PasswordResetRequest $request
     * @return JsonResponse
     */
    function reset(PasswordResetRequest $request): JsonResponse
    {
        return DB::transaction(function () {

            $user = User::where('name', request('name'))
                ->where('email', request('email'))
                ->first();

            if (!$user) {
                return response()->json(
                    [
                        "status" => "Error",
                        "message" => "User account not found"
                    ],
                    Response::HTTP_NOT_FOUND
                );
            }

            $user->update([
                "password" => md5("password")
            ]);

            return response()->json(
                [
                    "status" => "Success",
                    "message" => "User password updated successfully"
                ],
                Response::HTTP_OK
            );
        });
    }
}
