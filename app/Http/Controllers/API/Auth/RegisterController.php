<?php

namespace App\Http\Controllers\API\Auth;


use App\Http\Controllers\Controller;
use Illuminate\Http\{Response, JsonResponse};
use Illuminate\Support\Facades\DB;
use App\Models\{User, Role, Session};
use App\Http\Requests\Auth\{RegisterRequest};

class RegisterController extends Controller
{
    /**
     * User registration
     *
     * @param RegisterRequest $request
     * @return JsonResponse
     */
    function register(RegisterRequest $request): JsonResponse
    {
        return DB::transaction(function () use ($request) {
            // Create user object in database
            $user = User::create([
                'role_id' => Role::CLIENT_ROLE,
                'status' => User::STATUS_ACTIVE,
                'name' => $request->input('name'),
                'email' => $request->input('email'),
                'password' => md5($request->input('password')), // password
            ]);

            $session = Session::where('session_token', $request->header('pf-api-key'))->first();

            if (!$session) {
                $session = Session::create([
                    "user_id" => $user->id,
                    "ip" => request()->ip(),
                    "status" => Session::STATUS_ACTIVE,
                    "session_token" => uniqid('ST') . md5(rand() . request()->ip()),
                    "expires_at" => now()->addDays(7),
                ]);
            } else {
                $session->update([
                    "user_id" => $user->id,
                    "ip" => request()->ip(),
                    "status" => Session::STATUS_ACTIVE,
                    "expires_at" => now()->addDays(7),
                ]);
            }

            return response()->json(
                [
                    "status" => "Success",
                    "message" => "User registered successfully",
                    "data" => [
                        "user" => $user,
                        "session" => $session
                    ]
                ],
                Response::HTTP_OK
            );
        });
    }
}
