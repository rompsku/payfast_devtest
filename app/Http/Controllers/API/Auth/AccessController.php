<?php

namespace App\Http\Controllers\API\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\{Response, JsonResponse};
use Illuminate\Support\Facades\Validator;
use DB;
use App\Models\{User, Session};
use App\Http\Requests\Auth\{LoginRequest};

class AccessController extends Controller
{
    /**
     * User login
     *
     * @param RegisterRequest $request
     * @return JsonResponse
     */
    function login(LoginRequest $request): JsonResponse
    {

        try {
            // Start DB transaction
            DB::beginTransaction();

            // User validator to check required fields are submitted for login request
            $validator = Validator::make(
                request()->all(),
                [
                    'email' => 'required',
                    'password' => 'required',
                ]
            );

            // If validation fails, output a error response with details on validation errors
            if ($validator->fails()) {
                return response()->json(["status" => "Error", "message" => "Validation error <br/> " . $validator->errors(), "error" => $validator->errors()], 400);
            }

            // Get user based on login details submitted
            $user = User::where('email', request('email'))->first();

            // Check user exists
            if (!$user) {
                return response()->json(
                    [
                        "status" => "Error",
                        "message" => "Username not found"
                    ],
                    Response::HTTP_NOT_FOUND
                );
            } elseif ($user->password != md5(request('password'))) {
                // Check password is correct
                return response()->json(
                    [
                        "status" => "Error",
                        "message" => "Incorrect username / password combination."
                    ],
                    Response::HTTP_FORBIDDEN
                );
            } elseif (!$user->status) {
                // Check user account is active
                return response()->json(
                    [
                        "status" => "Error",
                        "message" => "User account inactive."
                    ],
                    Response::HTTP_FORBIDDEN
                );
            }

            // All good, check if user has active session
            $session = Session::where('user_id', $user->id)->where("expires_at", ">=", now())->where("status", SESSION::STATUS_ACTIVE)->first();

            if ($session) {
                // Update the session expiration
                $session->update(
                    [
                        "expires_at" => now()->addDays(7)
                    ]
                );
            } elseif (request()->header('pf-api-key')) {
                // If no active session, use the guest session we've setup on the request
                $session = Session::where("session_token", request()->header('pf-api-key'))->first();
                $session->update(
                    [
                        "user_id" => $user->id,
                        "expires_at" => now()->addDays(7)
                    ]
                );
            } else {
                // start new session if no active session is found
                $session = Session::create(
                    [
                        "user_id" => $user->id,
                        "ip" => request()->ip(),
                        "status" => Session::STATUS_ACTIVE,
                        "session_token" => uniqid('ST') . md5(rand() . request()->ip()),
                        "start" => now(),
                        "expires_at" => now()->addDays(7),
                    ]
                );
            }

            // Commit all database changes
            DB::commit();

            // respond with 200 OK and request data
            return response()->json(
                [
                    "status" => "Success",
                    "message" => "User session created successfully",
                    "data" => [
                        "user" => $user,
                        "session" => $session
                    ]
                ],
                Response::HTTP_OK
            );
        } catch (Exception $e) {
            // Roll back any DB changes
            DB::rollback();

            // Log errors
            report($e);

            // Respond with error message
            return response()->json(
                [
                    "status" => "Error",
                    "message" => "Server Error"
                ],
                500
            );
        }
    }

    /**
     * Attempt to destroy the current user session.
     *
     * @return void
     */
    function logout()
    {
        try {
            // Start DB transaction
            DB::beginTransaction();

            // Get session details from database if it's valid
            $session = Session::where('session_token', request()->header('pf-api-key'))
                ->where("expires_at", ">=", now())
                ->where("status", Session::STATUS_ACTIVE)
                ->first();

            // Check if session exists
            if ($session) {

                // Set to inactive
                $session->status = Session::STATUS_INACTIVE;
                $session->save();

                DB::commit();

                return response()->json(
                    [
                        "status" => true,
                        "message" => "Logout Successful"
                    ],
                    200
                );
            } else {
                // If not, go away
                return response()->json(
                    [
                        "status" => false,
                        "message" => "Session not found"
                    ],
                    403
                );
            }
        } catch (Exception $e) {
            // Roll back any DB changes
            DB::rollback();

            // Log errors
            report($e);

            // Respond with error message
            return response()->json(
                [
                    "status" => "Error",
                    "message" => "Server Error"
                ],
                500
            );
        }
    }
}
