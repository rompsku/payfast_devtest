<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\{Response, JsonResponse};
use DB;
use App\Models\{Movie};

class MovieController extends Controller
{
    /**
     * Retreive list of filtered movie objects
     *
     * @return JsonResponse
     */
    function index(): JsonResponse
    {

        /**
         * Initial builder instance.
         */
        $query = Movie::query();

        return response()->json(
            [
                "status" => "Success",
                "message" => "Data retrieved successfully",
                "data" => [
                    "movies" => $query->get(),
                    "count" => $query->count()
                ]
            ],
            Response::HTTP_OK,
        );
    }
}
