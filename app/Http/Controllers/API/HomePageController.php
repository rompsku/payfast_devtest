<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;

class HomePageController extends Controller
{
    /**
     * Undocumented function
     *
     * @param Request $request
     * @return void
     */
    public function index()
    {
        return view('api.homepage');
    }
}
