<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\{Response, JsonResponse};
use DB;
use App\Models\{Ticket, Booking};

class BookingController extends Controller
{
    /**
     * Create a new booking and tickets
     *
     * @return JsonResponse
     */
    function store(): JsonResponse
    {


        if (
            !request()->get('cinema_id') ||
            !request()->get('movie_id') ||
            !request()->get('time_slot_id') ||
            !request()->get('seat_ids') ||
            !request()->get('session')->user_id
        ) {
            return response()->json(
                [
                    "status" => "Error",
                    "message" => "Invalid request"
                ],
                Response::HTTP_INTERNAL_SERVER_ERROR,
            );
        }

        $booking = Booking::create([
            'user_id' => request()->get('session')->user_id,
            'booking_reference' => uniqid("BK-"),
            'timeslot_id' => request()->get('time_slot_id')
        ]);

        $seat_ids = explode(",", request()->get('seat_ids'));
        foreach ($seat_ids as $seat_id) {
            Ticket::updateOrCreate([
                'booking_id' => $booking->id,
                'seat_id' => $seat_id
            ]);
        }

        return response()->json(
            [
                "status" => "Success",
                "message" => "Booking created successfully",
                "data" => []
            ],
            Response::HTTP_OK,
        );
    }

    /**
     * Delete a booking and tickets
     *
     * @return JsonResponse
     */
    function delete(): JsonResponse
    {
        if (
            !request()->get('booking_id') ||
            !request()->get('session')->user_id
        ) {
            return response()->json(
                [
                    "status" => "Error",
                    "message" => "Invalid request"
                ],
                Response::HTTP_INTERNAL_SERVER_ERROR,
            );
        }

        $booking = Booking::with('timeslot')->whereId(request()->get('booking_id'))->first();
        if(strtotime($booking->timeslot->starts_at) < strtotime("-1 hour")){
            return response()->json(
                [
                    "status" => "Error",
                    "message" => "Booking has passed cancellation grace period. you can no longer cancel this booking."
                ],
                Response::HTTP_INTERNAL_SERVER_ERROR,
            );
        }

        Booking::whereId(request()->get('booking_id'))->delete();
        Ticket::whereBookingId(request()->get('booking_id'))->delete();

        return response()->json(
            [
                "status" => "Success",
                "message" => "Booking created successfully"
            ],
            Response::HTTP_OK,
        );
    }
}
