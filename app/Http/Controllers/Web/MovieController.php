<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use DB;
use App\Models\{Movie};
use App\Http\Traits\TimeslotTrait;

class MovieController extends Controller
{

    use TimeslotTrait;
    /**
     * Undocumented function
     *
     * @param Request $request
     * @return void
     */
    public function index()
    {
        return view(
            'web.movies.list',
            [
                "movies" => Movie::with('timeslots')->orderBy('created_at', 'DESC')->get(),
                "next_showing" => $this->nextShowing(),
                'menu_item' => 'movies'
            ]
        );
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     * @return void
     */
    public function show($id)
    {
        return view(
            'web.movies.single',
            [
                "movie" => Movie::with('timeslots')->orderBy('created_at', 'DESC')->whereId($id)->firstOrFail(),
                "next_showing" => $this->nextShowing(),
                'menu_item' => 'movies'
            ]
        );
    }
}
