<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use DB;
use App\Models\{Movie, Timeslot, Cinema};
use App\Http\Traits\TimeslotTrait;

class HomePageController extends Controller
{
    use TimeslotTrait;

    /**
     * Undocumented function
     *
     * @param Request $request
     * @return void
     */
    public function index()
    {
        $movie = Movie::inRandomOrder()->first();
        $cinema = Cinema::inRandomOrder()->first();

        return view(
            'web.homepage',
            [
                "movie" => $movie,
                "cinema" => $cinema,
                "next_showing" => $this->nextShowing(),
                'menu_item' => 'home'
            ]
        );
    }
}
