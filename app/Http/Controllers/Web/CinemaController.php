<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use DB;
use App\Models\{Cinema};
use App\Http\Traits\TimeslotTrait;

class CinemaController extends Controller
{

    use TimeslotTrait;
    /**
     * Undocumented function
     *
     * @param Request $request
     * @return void
     */
    public function index()
    {
        return view(
            'web.cinemas.list',
            [
                "cinemas" => array_chunk(Cinema::withCount('theatres')->orderBy('created_at', 'DESC')->get()->toArray(), 2),
                "next_showing" => $this->nextShowing(),
                'menu_item' => 'cinemas'
            ]
        );
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     * @return void
     */
    public function show($id)
    {
        return view(
            'web.cinemas.single',
            [
                "cinema" => Cinema::withCount('timeslots')->orderBy('created_at', 'DESC')->whereId($id)->firstOrFail(),
                "next_showing" => $this->nextShowing(),
                'menu_item' => 'cinemas'
            ]
        );
    }
}
