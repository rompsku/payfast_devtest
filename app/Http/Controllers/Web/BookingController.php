<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Http\Traits\TimeslotTrait;
use App\Models\{Cinema, Movie, Booking};

class BookingController extends Controller
{
    use TimeslotTrait;

    /**
     * Undocumented function
     *
     * @param Request $request
     * @return void
     */
    public function index()
    {

        return view(
            'web.my-bookings',
            [
                "bookings" => Booking::with(['timeslot', 'tickets'])->whereUserId(request()->get('session')->user_id)->get(),
                "next_showing" => $this->nextShowing(),
                'menu_item' => 'my-bookings'
            ]
        );
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     * @return void
     */
    public function create()
    {

        return view(
            'web.booking',
            [
                "cinema" => (request()->has('c') ? Cinema::whereId(request()->get('c'))->first() : false),
                "movie" => (request()->has('m') ? Movie::whereId(request()->get('m'))->first() : false),
                "next_showing" => $this->nextShowing(),
                'menu_item' => 'home'
            ]
        );
    }
}
