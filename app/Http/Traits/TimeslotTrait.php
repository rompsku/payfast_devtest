<?php

namespace App\Http\Traits;

use App\Models\Timeslot;

trait TimeslotTrait
{
    public function nextShowing(): Timeslot
    {
        // Fetch next showing movie
        return Timeslot::with(['movie', 'theatre'])
            ->where('starts_at', '>', now()->addHours(1))
            ->orderBy('starts_at', 'ASC')
            ->first();
    }
}
