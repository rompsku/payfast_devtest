<?php

namespace App\Http\Middleware\Custom;

use Closure;
use App\Models\Session;

class WebAuth
{
    /**
     * Handle an incoming request on API routes
     * All routes should require the user sending a session token.
     * If no token is present, error out the request
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $headers = $request->header();
        $session = $this->getUserSession();
        $pf_api_key = null;

        if (empty($_COOKIE['pf-api-key']) || $_COOKIE['pf-api-key'] !== $session->session_token) {
            setcookie('pf-api-key', $session->session_token, time() + 24 * 60 * 60, '/');
        }

        // Check for api token first
        if (isset($headers['pf_api_key'])) {

            $pf_api_key = (isset($headers['pf_api_key']) ? $headers['pf_api_key'] : $_COOKIE['pf_api_key']);

            // Get user session based on session token
            $session = Session::with('user')->where('session_token', $pf_api_key)->where("status", Session::STATUS_ACTIVE)->first();

            // Check if session exists
            if ($session) {

                // if session check if session is still active
                if (strtotime($session->expires_at) >= now()) {

                    $session->expires_at = now()->addDays(2);
                    $session->save();
                } else {

                    // If not, manually set to expired and null the variable
                    $session->save();
                    $session = null;
                }
            }
        }

        // If there is an no active session found, create a guest one
        if (!$session) {

            // start new session if no active session is found
            $session = Session::create(
                [
                    "user_id" => NULL,
                    "ip" => request()->ip(),
                    "status" => Session::STATUS_ACTIVE,
                    "session_token" => uniqid('ST') . md5(rand() . request()->ip()),
                    "start" => now(),
                    "expires_at" => now()->addDays(1),
                ]
            );
        }

        $request->attributes->add(['session' => $session]);

        return $next($request);
    }

    private function getUserSession()
    {
        $session = null;

        if (isset($_COOKIE['pf-api-key'])) {
            $session =  Session::where('session_token', $_COOKIE['pf-api-key'])
                ->where("status", SESSION::STATUS_ACTIVE)
                ->where("expires_at", ">=", now())
                ->orderBy('expires_at', 'desc')
                ->with('user')
                ->first();
        }

        if (!$session) {
            $session = Session::create([
                "user_id" => $session->user->user_id ?? null,
                "ip" => request()->ip(),
                "status" => Session::STATUS_ACTIVE,
                "session_token" => uniqid('ST') . md5(rand() . request()->ip()),
                "expires_at" => now()->addHours(24),
            ]);
        }


        
        setcookie('pf-api-key', $session->session_token, time() + 24 * 60 * 60, '/');

        return $session;
    }
}
