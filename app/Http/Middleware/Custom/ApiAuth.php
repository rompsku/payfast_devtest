<?php

namespace App\Http\Middleware\Custom;

use Closure;
use App\Models\Session;
use Carbon\Carbon;
use Illuminate\Http\{Response};

class ApiAuth
{
    /**
     * Handle an incoming request on API routes
     * All routes should require the user sending a session token.
     * If no token is present, error out the request
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $headers = $request->header();
        $session = null;
        $pf_api_key = null;

        // Check for api token first
        if (isset($headers['pf-api-key'])) {

            $pf_api_key = (isset($headers['pf-api-key']) ? $headers['pf-api-key'] : $_COOKIE['pf-api-key']);

            // Get user session based on session token
            $session = Session::with('user')->where('session_token', $pf_api_key)->where("status", Session::STATUS_ACTIVE)->first();

            
            // Check if session exists
            if ($session) {

                // if session check if session is still active
                if (Carbon::parse($session->expires_at) >= now()) {
                    $session->expires_at = now()->addDays(2);
                    $session->save();
                } else {
                    // If not, manually set to expired and null the variable
                    $session->save();
                    $session = null;
                }
            }
        }

        // If there is an no active session found, 403 out
        if (!$session) {
            return response()->json(
                [
                    "status" => "Error",
                    "message" => "Unauthorized access attempt."
                ],
                Response::HTTP_FORBIDDEN
            );
        }

        $request->attributes->add(['session' => $session]);
        return $next($request);
    }
}
