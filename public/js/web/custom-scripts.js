//
var cinemas = false;
var movies = false;
var timeslots = false;
var seats = false;
var selected_seats = [];

//This function is used to get Cookie
function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(";");
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == " ") {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

//
function numToSSColumn(num) {
    var s = '',
        t;

    while (num > 0) {
        t = (num - 1) % 26;
        s = String.fromCharCode(65 + t) + s;
        num = (num - t) / 26 | 0;
    }
    return s || undefined;
}

function isEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}

function openLoginBlock() {
    $('.register-block').fadeOut();
    $('.login-block').fadeIn();

    $('#register-link').removeClass('pfdemo-active');
    $('#login-link').addClass('pfdemo-active');
}

function openRegisterBlock() {
    $('.login-block').fadeOut();
    $('.register-block').fadeIn();

    $('#login-link').removeClass('pfdemo-active');
    $('#register-link').addClass('pfdemo-active');
}

function logoutUser() {
    $.ajax({
        url: api_url + "/auth/logout",
        type: "get",
        headers: {
            "pf-api-key": pf_cookie
        },
        success: function(response) {

            $('#auth-messages').html('Logout successful! Page will reload shortly...');

            setTimeout(function() {
                window.location.reload(1);
            }, 3000);
        },
        error: function(response) {
            $('#auth-messages').html('Logout failed.');
        }
    })
}

function submitLoginForm() {

    $('#auth-messages').html("");

    if (!$("#login_email").val()) {
        $('#auth-messages').html("Email required.");
        return false;
    }
    if (!$("#login_password").val()) {
        $('#auth-messages').html("Password is required.");
        return false;
    }

    $.ajax({
        url: api_url + "/auth/login",
        type: "post",
        data: {
            email: $("#login_email").val(),
            password: $("#login_password").val()
        },
        headers: {
            "pf-api-key": pf_cookie
        },
        dataType: "json",
        success: function(response) {

            $('#auth-messages').html('Login successful! Page will reload shortly...');

            setTimeout(function() {
                window.location.reload(1);
            }, 3000);
        },
        error: function(response) {

            $('#auth-messages').html(response.message);
            console.log(response);
        }
    })
}

function submitRegistrationForm() {

    $('#auth-messages').html("");

    if (!$("#register_name").val()) {
        $('#auth-messages').html("Name required.");
        return false;
    }
    if (!$("#register_email").val()) {
        $('#auth-messages').html("Email required.");
        return false;
    }
    if (!isEmail($("#register_email").val())) {
        $('#auth-messages').html("Invalid email address.");
        return false;
    }
    if (!$("#register_password").val()) {
        $('#auth-messages').html("Password is required.");
        return false;
    }
    if ($("#register_password").val() != $("#register_confirm_password").val()) {
        $('#auth-messages').html("Passwords do not match.");
        return false;
    }

    $.ajax({
        url: api_url + "/auth/register",
        type: "post",
        data: {
            name: $("#register_name").val(),
            email: $("#register_email").val(),
            password: $("#register_password").val()
        },
        headers: {
            "pf-api-key": pf_cookie
        },
        dataType: "json",
        success: function(response) {

            $('#auth-messages').html('Registration successful! Page will reload shortly...');

            setTimeout(function() {
                window.location.reload(1);
            }, 3000);
        },
        error: function(response) {

            $('#auth-messages').html(response.responseJSON.message);
            console.log(response);
        }
    })
}

function resetBookingTimeslots() {

    //
    $('#bookingTimeslotBG').css("background-image", "");
    $('#bookingTimeslotDateTime').html("");
    $('#bookingTimeslotTheatre').html("");
    $('#booking_form_time_slot_id').val("");
    $('#bookingTimeslotChangeButton').html('Select');

    if ($('#booking_form_cinema_id').val() != '' || $('#booking_form_movie_id').val() != '') {
        $('#bookingTimeslotChangeButton').removeClass('greyed-out');
    } else {
        $('#bookingTimeslotChangeButton').addClass('greyed-out');
    }

    timeslots = false;
}

function resetBookingSeats() {
    $('#bookingSeatBG').css("background-image", "");
    $('#bookingSeatAmount').html("0");
    $('#booking_form_seat_ids').val("");
    $('#bookingSeatChangeButton').html('Select');
    $('#bookingSeatChangeButton').addClass('greyed-out');

    seats = false;
}

function updateBooking(type, selection) {

    if (type == 'cinema') {
        $('#bookingCinemaBG').css("background-image", "url(" + selection.image_exterior + ")");
        $('#bookingCinemaTitle').html(selection.title);
        $('#bookingCinemaAddress').html(selection.address);
        $('#booking_form_cinema_id').val(selection.id);
        $('#bookingCinemaChangeButton').html('Change')

        resetBookingTimeslots();
        resetBookingSeats();

        document.getElementById("bookingModal").style.display = 'none';

    } else if (type == 'movie') {
        $('#bookingMovieBG').css("background-image", "url(" + selection.poster + ")");
        $('#bookingMovieTitle').html(selection.title);
        $('#bookingMovieRating').html(selection.rating);
        $('#bookingMovieRuntime').html(selection.runtime);
        $('#bookingMovieLanguage').html(selection.language);
        $('#booking_form_movie_id').val(selection.id);
        $('#bookingMovieChangeButton').html('Change')

        resetBookingTimeslots();
        resetBookingSeats();

        document.getElementById("bookingModal").style.display = 'none';

    } else if (type == 'timeslot') {
        $('#bookingTimeslotBG').css("background-image", "url(/images/showcases.jpg)");
        $('#bookingTimeslotDateTime').html(selection.starts_at);
        $('#bookingTimeslotTheatre').html(selection.theatre.title);
        $('#booking_form_time_slot_id').val(selection.id);
        $('#bookingTimeslotChangeButton').html('Change');

        resetBookingSeats();

        $('#bookingSeatAmount').html(0);
        $('#bookingSeatChangeButton').removeClass('greyed-out');

        document.getElementById("bookingModal").style.display = 'none';

    } else if (type == 'submit') {

    }

    return false;
}

function launchCinemaSelectionModal() {
    //
    $('.model-content-inner').html("<div class='loading-placeholder'></div>");
    document.getElementById("bookingModal").style.display = 'block';

    $.ajax({
        url: api_url + "/web/cinemas",
        type: "get",
        headers: {
            "pf-api-key": pf_cookie
        },
        success: function(response) {
            console.log(response);
            cinemas = response.data.cinemas;
            $('.model-content-inner').html("<h2>Select a Cinema...</h2><hr/><div class='row' id='cinema-selection-block'></div>");
            for (i in cinemas) {
                $('.model-content-inner #cinema-selection-block').append(`<div class='col-6 col-md-4 booking-cinema-selection-item' onclick="updateBooking('cinema', cinemas[${i}])">
                    <img src='${cinemas[i].image_exterior}' />
                    <strong>${cinemas[i].title}</strong>
                    <p>${cinemas[i].address}</p>
                </div>`);
            }
        },
        error: function(response) {
            $('.model-content-inner').html(response.responseJSON.message);
            console.log(response);
        }
    })
}

function launchMovieSelectionModal() {
    //
    $('.model-content-inner').html("<div class='loading-placeholder'></div>");
    document.getElementById("bookingModal").style.display = 'block';

    $.ajax({
        url: api_url + "/web/movies",
        type: "get",
        headers: {
            "pf-api-key": pf_cookie
        },
        success: function(response) {
            console.log(response);
            movies = response.data.movies;
            $('.model-content-inner').html("<h2>Select a Movie...</h2><hr/><div class='row' id='movie-selection-block'></div>");
            for (i in movies) {
                $('.model-content-inner #movie-selection-block').append(`<div class='col-4 col-md-3 booking-movie-selection-item' onclick="updateBooking('movie', movies[${i}])">
                    <img src='${movies[i].poster}' />
                    <strong>${movies[i].title}</strong>
                    <p>Rated: ${movies[i].age_rating}<br/>
                    Runtime: ${movies[i].runtime}<br/>
                    Languages: ${movies[i].languages}</p>
                </div>`);
            }
        },
        error: function(response) {
            $('.model-content-inner').html(response.responseJSON.message);
            console.log(response);
        }
    })
}

function launchTimeslotSelectionModal() {

    //
    if ($('#booking_form_cinema_id').val() == '' || $('#booking_form_movie_id').val() == '')
        return false;

    //
    $('.model-content-inner').html("<div class='loading-placeholder'></div>");
    document.getElementById("bookingModal").style.display = 'block';

    $.ajax({
        url: api_url + "/web/timeslots?" +
            "cinema_id=" + $('#booking_form_cinema_id').val() +
            "&movie_id=" + $('#booking_form_movie_id').val(),
        type: "get",
        headers: {
            "pf-api-key": pf_cookie
        },
        success: function(response) {
            timeslots = response.data.timeslots;
            if (timeslots.length) {
                $('.model-content-inner').html("<h2>Select a Time slot...</h2><hr/><div class='row' id='timeslot-selection-block'></div>");
                for (i in timeslots) {
                    $('.model-content-inner #timeslot-selection-block').append(`<div class='col-6 col-md-4 booking-timeslot-selection-item' onclick="updateBooking('timeslot', timeslots[${i}])">
                    <strong>${timeslots[i].starts_at}</strong>
                    <br/>
                    ${timeslots[i].theatre.title}
                </div>`);
                }
            } else {
                $('.model-content-inner').html("<h2>No timeslots found. :(</h2><p>No available time slots were found for the movie and cinema combination you have select. Please try selecting a different cinema or movie.</div>");
            }
        },
        error: function(response) {
            $('.model-content-inner').html(response.responseJSON.message);
            console.log(response);
        }
    })
}

function launchSeatSelectionModal() {

    $('#bookingSeatBG').css("background-image", "url()");
    //
    if (!$('#booking_form_time_slot_id').val())
        return false;

    //
    $('.model-content-inner').html("<div class='loading-placeholder'></div>");
    document.getElementById("bookingModal").style.display = 'block';
    selected_seats = [];

    if (!seats.length) {
        $.ajax({
            url: api_url + "/web/seats?" + "timeslot_id=" + $('#booking_form_time_slot_id').val(),
            type: "get",
            headers: {
                "pf-api-key": pf_cookie
            },
            success: function(response) {
                seats = response.data.seats;
                if (seats.length) {
                    $('.model-content-inner').html("<h2>Select your seats...</h2><hr/><div class='row' id='seat-selection-block'></div>");
                    for (i in seats) {
                        $('.model-content-inner #seat-selection-block').append(`<div class='col-3 col-md-2 booking-seat-selection-item' onclick="updateBooking('seat', seats[${i}])">
                    <strong ${(seats[i].tickets.length ? 'class="reserved"' : '')} id="seat${seats[i].id}" onclick="toggleSelectedSeat(${seats[i].id})">${numToSSColumn(seats[i].location_x)}${seats[i].location_y} ${(seats[i].tickets.length ? '<br/> <span>RESERVED</span>' : '')}</strong>
                </div>`);
                    }
                } else {
                    $('.model-content-inner').html("<h1>No seats found. :(</h1><p>No available seats were found for the time slot you have select. Please try selecting a different time slot.</div>");
                }
            },
            error: function(response) {
                $('.model-content-inner').html(response.responseJSON.message);
                console.log(response);
            }
        });
    } else {
        if (seats.length) {
            $('.model-content-inner').html("<h2>Select your seats...</h2><hr/><div class='row' id='seat-selection-block'></div>");
            for (i in seats) {
                $('.model-content-inner #seat-selection-block').append(`<div class='col-3 col-md-2 booking-seat-selection-item' onclick="updateBooking('seat', seats[${i}])">
            <strong ${(seats[i].tickets.length ? 'class="reserved"' : '')} id="seat${seats[i].id}" onclick="toggleSelectedSeat(${seats[i].id})">${numToSSColumn(seats[i].location_x)}${seats[i].location_y} ${(seats[i].tickets.length ? '<br/> <span>RESERVED</span>' : '')}</strong>
        </div>`);
            }
        } else {
            $('.model-content-inner').html("<h2>No seats found. :(</h2><p>No available seats were found for the time slot you have select. Please try selecting a different time slot.</div>");
        }
    }
}

function toggleSelectedSeat(seat_id) {
    if (!$('#seat' + seat_id).hasClass('reserved')) {
        console.log(seat_id);
        if (selected_seats.includes(seat_id)) {
            var index = selected_seats.indexOf(seat_id);
            if (index !== -1) {
                selected_seats.splice(index, 1);
            }
        } else {
            selected_seats.push(seat_id);
        }
        $('#seat' + seat_id).toggleClass('selected');

        $('#bookingSeatAmount').html(selected_seats.length);
        $('#booking_form_seat_ids').val(selected_seats.join(","));


        if (selected_seats.length > 0) {
            $('#bookingSeatChangeButton').html('Change');
            $('#bookingSeatBG').css("background-image", "url(/images/seats.jpg)");
            $('#bookingCompleteBtn').removeClass('greyed-out');

        } else {
            $('#bookingSeatChangeButton').html('Select');
            $('#bookingSeatBG').css("background-image", "url()");
            $('#bookingCompleteBtn').addClass('greyed-out');
        }
    }
}

function completeBooking() {
    if (!$('#booking_form_cinema_id').val() ||
        !$('#booking_form_movie_id').val() ||
        !$('#booking_form_time_slot_id').val() ||
        !$('#booking_form_seat_ids').val())
        return false;

    //
    $('.model-content-inner').html("<div class='loading-placeholder'></div>");
    document.getElementById("bookingModal").style.display = 'block';
    selected_seats = [];

    $.ajax({
        url: api_url + "/web/bookings",
        type: "post",
        data: {
            cinema_id: $("#booking_form_cinema_id").val(),
            movie_id: $("#booking_form_movie_id").val(),
            time_slot_id: $("#booking_form_time_slot_id").val(),
            seat_ids: $("#booking_form_seat_ids").val()
        },
        headers: {
            "pf-api-key": pf_cookie
        },
        dataType: "json",
        success: function(response) {
            $('.model-content-inner').html("<h2>Booking Successful!</h2><p>Your requested booking has been created. <br/> You wll be redirect to your bookings page shortly...</div>");


            setTimeout(function() {
                window.location = '/my-bookings';
            }, 3000);
        },
        error: function(response) {
            $('.model-content-inner').html("<h2>Booking Error :(</h2><p>The requested booking could not be created.</div>");

            $('.model-content-inner').html(response.responseJson.message);
            console.log(response);
        }
    })


}

function cancelBooking(booking_id) {

    if (!$("#cancelBookingBtn" + booking_id).hasClass('are-you-sure')) {
        $("#cancelBookingBtn" + booking_id).addClass('are-you-sure');
        $("#cancelBookingBtn" + booking_id).html('Are You Sure?');
    } else {
        $.ajax({
            url: api_url + "/web/bookings/cancel",
            type: "post",
            data: {
                booking_id: booking_id,
            },
            headers: {
                "pf-api-key": pf_cookie
            },
            dataType: "json",
            success: function(response) {
                $("#cancelBooking" + booking_id).fadeOut();
            },
            error: function(response) {
                console.log(response);
            }
        })
    }
}

// Get the modal and close button
var modal = document.getElementById("bookingModal");
var span = document.getElementsByClassName("modal-close")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}