const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */


mix.js('resources/js/app.js', 'public/js/web/app.js').vue();

mix.sass('resources/scss/app.scss', 'public/css/web/custom-styles.css').version();
mix.scripts('resources/js/custom.js', 'public/js/web/custom-scripts.js').version();