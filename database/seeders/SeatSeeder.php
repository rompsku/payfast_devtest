<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\{Theatre, Seat};

class SeatSeeder extends Seeder
{


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $theatres = Theatre::get();
        //
        foreach ($theatres as $theatre) {

            //
            $x_end = 6;
            $y_end = 5;

            for ($x = 1; $x <= $x_end; $x++) {
               for ($y = 1; $y <= $y_end; $y++) {
                    Seat::create(
                        [
                            'theatre_id' => $theatre->id,
                            'status' => Seat::STATUS_ACTIVE,
                            'location_x' => $x,
                            'location_y' => $y
                        ]
                    );
                    $y++;
                }
            }
        }
    }
}
