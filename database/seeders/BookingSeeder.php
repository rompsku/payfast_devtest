<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\{Timeslot, Booking, User, Seat, Ticket};
use Carbon\Carbon;

class BookingSeeder extends Seeder
{


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $timeslots = Timeslot::with(['theatre'])->get();
        foreach ($timeslots as $timeslot) {

            //
            $bookings_i = 0;
            $booking_amount = rand(1, 5);

            //
            while ($bookings_i < $booking_amount) {
                //
                $bookings_i++;

                //
                $available_seats = Seat::whereDoesntHave('tickets', function ($query) use ($timeslot) {
                    $query->whereHas('booking', function ($query) use ($timeslot) {
                        $query->where('timeslot_id', $timeslot->id);
                    });
                })->inRandomOrder()->limit(rand(1, 5))->get();

                if ($available_seats) {
                    $user = User::whereStatus(User::STATUS_ACTIVE)->inRandomOrder()->first();
                    $booking = Booking::create([
                        'user_id' => $user->id,
                        'booking_reference' => uniqid("BK-"),
                        'timeslot_id' => $timeslot->id
                    ]);

                    foreach ($available_seats as $seat) {
                        Ticket::create([
                            'booking_id' => $booking->id,
                            'seat_id' => $seat->id
                        ]);
                    }
                }
            }
        }
    }
}
