<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Cinema;

class CinemaSeeder extends Seeder
{


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Cinema::insert(
            [
                [
                    'title' => 'GoDrive Roadhouse',
                    'status' => 1,
                    'geo_location' => "-33.934384420427655, 18.45923855385995",
                    'address' => "346B Victoria Rd, Salt River, Cape Town, 7925",
                    'description' => 'GoDriveIn Movie & Roadhouse in Salt River will be bringing you a variety of big-screen entertainment that you can enjoy right from the comfort and safety of your car. GoDriveIn is a Movie & Roadhouse venue in Cape Town with an offering of food trucks.',
                    'image_thumb' => "https://devsecure.co.za/resources/pftest/images/cinema-thumb-1.jpg",
                    'image_interior' => "https://devsecure.co.za/resources/pftest/images/seats.jpg",
                    'image_exterior' => "https://devsecure.co.za/resources/pftest/images/cinema-ext-1.jpg",
                    'image_large' => "https://devsecure.co.za/resources/pftest/images/cinema-lg-1.webp",
                ],
                [
                    'title' => 'CineCentre',
                    'status' => 1,
                    'geo_location' => "-33.91734137913711, 18.54644128772009",
                    'address' => "GrandWest Casino and Entertainment World, 1 Jakes Gerwel Dr, Goodwood, Cape Town, 7460",
                    'description' => 'CineCentre in Goodwood will be bringing you a variety of big-screen entertainment.',
                    'image_thumb' => "https://devsecure.co.za/resources/pftest/images/cinema-thumb-2.webp",
                    'image_interior' => "https://devsecure.co.za/resources/pftest/images/seats.jpg",
                    'image_exterior' => "https://devsecure.co.za/resources/pftest/images/cinema-ext-2.webp",
                    'image_large' => "https://devsecure.co.za/resources/pftest/images/cinema-lg-2.jpg",

                ],
                [
                    'title' => 'The Galileo',
                    'status' => 1,
                    'geo_location' => "-33.986971955814106, 18.431642738889355",
                    'address' => "0A Rhodes Ave, Wynberg NU (2), Cape Town, 7800",
                    'description' => 'Treat yourself to cinematic delights, tantalizing treats and breath-taking scenery under a canopy of stars.',
                    'image_thumb' => "https://devsecure.co.za/resources/pftest/images/cinema-thumb-3.webp",
                    'image_interior' => "https://devsecure.co.za/resources/pftest/images/seats.jpg",
                    'image_exterior' => "https://devsecure.co.za/resources/pftest/images/cinema-ext-3.jpg",
                    'image_large' => "https://devsecure.co.za/resources/pftest/images/cinema-lg-3.jpg",

                ],
                [
                    'title' => 'The Labia',
                    'status' => 1,
                    'geo_location' => "-33.928879214932095, 18.412588325459733",
                    'address' => "0A Rhodes Ave, Wynberg NU (2), Cape Town, 7800",
                    'description' => 'The Labia Theatre, originally an Italian Embassy ballroom, was opened by Princess Labia in May 1949 as a theatre for the staging of live performances.',
                    'image_thumb' => "https://devsecure.co.za/resources/pftest/images/cinema-thumb-4.jpg",
                    'image_interior' => "https://devsecure.co.za/resources/pftest/images/seats.jpg",
                    'image_exterior' => "https://devsecure.co.za/resources/pftest/images/cinema-ext-4.jpg",
                    'image_large' => "https://devsecure.co.za/resources/pftest/images/cinema-lg-4.jpg",

                ],
                [
                    'title' => 'Ster Kinekor - Blue Route',
                    'status' => 1,
                    'geo_location' => "-34.06375296251505, 18.451757239298015",
                    'address' => "16 Tokai Rd, Dreyersdal, Cape Town, 7945",
                    'description' => 'To most South Africans, the word “movies” is synonymous with the name “Ster-Kinekor”; the biggest movie exhibitor by far (more than double the size of it’s nearest competitor), Ster-Kinekor owns some 60-65% of the market, boasting some 55 movie complexes in South Africa (more than 400 screens and 64 000 seats) with 154 state-of-the-art 3D cinemas, giving Ster-Kinekor the largest 3D footprint in this territory, as well as 6 complexes in neighbouring territories.',
                    'image_thumb' => "https://devsecure.co.za/resources/pftest/images/cinema-thumb-5.jpg",
                    'image_interior' => "https://devsecure.co.za/resources/pftest/images/seats.jpg",
                    'image_exterior' => "https://devsecure.co.za/resources/pftest/images/cinema-ext-5.jpg",
                    'image_large' => "https://devsecure.co.za/resources/pftest/images/cinema-lg-5.jpg",

                ],
                [
                    'title' => 'Nu Metro - Blue Route',
                    'status' => 1,
                    'geo_location' => "-33.892206112300094, 18.510906850979428",
                    'address' => "479, Century Blvd, Century City, Cape Town, 7441",
                    'description' => 'To most South Africans, the word “movies” is synonymous with the name “Nu Metro”; the biggest movie exhibitor by far (more than double the size of it’s nearest competitor), Nu Metro owns some 60-65% of the market, boasting some 55 movie complexes in South Africa (more than 400 screens and 64 000 seats) with 154 state-of-the-art 3D cinemas, giving Nu Metro the largest 3D footprint in this territory, as well as 6 complexes in neighbouring territories.',
                    'image_thumb' => "https://devsecure.co.za/resources/pftest/images/cinema-thumb-6.jpg",
                    'image_interior' => "https://devsecure.co.za/resources/pftest/images/seats.jpg",
                    'image_exterior' => "https://devsecure.co.za/resources/pftest/images/cinema-ext-6.jpg",
                    'image_large' => "https://devsecure.co.za/resources/pftest/images/cinema-lg-6.jpg",

                ]
            ]
        );
    }
}
