<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        $this->call([
            \Database\Seeders\RolesSeeder::class,
            \Database\Seeders\Users\AdminUserSeeder::class,
            \Database\Seeders\Users\ClientUserSeeder::class,
            \Database\Seeders\MoviesSeeder::class,
            \Database\Seeders\CinemaSeeder::class,
            \Database\Seeders\TheatreSeeder::class,
            \Database\Seeders\TimeslotSeeder::class,
            \Database\Seeders\SeatSeeder::class,
            \Database\Seeders\BookingSeeder::class
        ]);
    }
}
