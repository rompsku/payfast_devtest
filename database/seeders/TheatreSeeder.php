<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\{Cinema,Theatre};

class TheatreSeeder extends Seeder
{


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cinemas = Cinema::get();
        //
        foreach ($cinemas as $cinema) {
            $i = 1;
            while ($i <= rand(1, 5)) {
                Theatre::create(
                    [
                        'cinema_id' => $cinema->id,
                        'status' => 1,
                        'title' => "Screen " . $i,

                    ]
                );

                $i++;
            }
        }
    }
}
