<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\{Theatre, Timeslot, Movie};

class TimeslotSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $day_count = 30;
        $day_i = 0;
        $screening_timeslots = ["10", "15", "20"]; // 

        while ($day_i <= $day_count) {
            $theatres = Theatre::get();
            //
            foreach ($theatres as $theatre) {
                $i = 0;
                $screenings_count = rand(0, 2);
                while ($i < $screenings_count) {
                    //
                    $movie = Movie::inRandomOrder()->first();
                    Timeslot::create(
                        [
                            'theatre_id' => $theatre->id,
                            'movie_id' => $movie->id,
                            'reserved_seating' => rand(0, 1),
                            'starts_at' => now()->addDays($day_i)->startOfDay()->addMinutes(($screening_timeslots[$i] * 60)),

                        ]
                    );
                    $i++;
                }
            }

            $day_i++;
        }
    }
}
