<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Movie;

class MoviesSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Provide a bunch of IMDB movie IDs, the seeder will import their data
        $imdb_ids = [
            "DUNE" => "tt1160419",
            "Licorice Pizza" => "tt11271038",
            "A Quiet Place Part II" => "tt1285016",
            "Cruella" => "tt3228774",
            "Pig" => "tt11003218",
            "The Last Duel" => "tt4244994",
            "Raya and the Last Dragon" => "tt5109280",
            "King Richard" => "tt9620288",
            "House of Gucci" => "tt11214590",
            "Don't Look Up" => "tt11286314",
            "Luca" => "tt12801262",
            "Encanto" => "tt2953050",
            "Spiderman: No Way Home" => "tt3480822",
            "The Eternals" => "tt9032400",
            "Moxie" => "tt6432466",
            "Black Widow" => "tt3480822",
            "No Time To Die" => "tt2382320",
            "Old" => "tt10954652",
            "Kate" => "tt7737528",
            "Bruised" => "tt8310474",
            "Malignant" => "tt3811906",
        ];



        foreach ($imdb_ids as $k => $v) {

            $imdb_data = json_decode(file_get_contents("https://www.omdbapi.com/?i=" . $v . "&r=json&plot=full&apikey=" . config("app.omdb_api_key")));

            if (!isset($imdb_data->Title))
                continue;

            //
            Movie::create([
                'title' => $imdb_data->Title,
                'status' => Movie::STATUS_ACTIVE,
                'age_rating' => $imdb_data->Rated,
                'runtime' => $imdb_data->Runtime,
                'plot' => $imdb_data->Plot,
                'genres' => $imdb_data->Genre,
                'director' => $imdb_data->Director,
                'writer' => $imdb_data->Writer,
                'actors' => $imdb_data->Actors,
                'languages' => $imdb_data->Language,
                'countries' => $imdb_data->Country,
                'awards' => $imdb_data->Awards,
                'poster' => $imdb_data->Poster,
                'imdb_rating' => $imdb_data->imdbRating,
                'imdb_id' => $imdb_data->imdbID,
                'box_office' => $imdb_data->BoxOffice,
                'release_date' => $imdb_data->Released,
            ]);
        }
    }
}
