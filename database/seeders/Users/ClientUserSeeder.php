<?php

namespace Database\Seeders\Users;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\{User, Role};

class ClientUserSeeder extends Seeder
{


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // Active, verified user
        User::create([
            'role_id' => Role::CLIENT_ROLE,
            'status' => User::STATUS_ACTIVE,
            'name' => "test_active_user",
            'email' => "test_active_user@payfasttest.com",
            'password' => md5('password'), // password
        ]);

        // Inactive, verified user
        User::create([
            'role_id' => Role::CLIENT_ROLE,
            'status' => User::STATUS_INACTIVE,
            'name' => "test_inactive_user",
            'email' => "test_inactive_user@payfasttest.com",
            'password' => md5('password'), // password
        ]);

        // Unverified user
        User::create([
            'role_id' => Role::CLIENT_ROLE,
            'status' => User::STATUS_INACTIVE,
            'name' => "test_unverified_user",
            'email' => "test_unverified_user@payfasttest.com",
            'password' => md5('password'), // password
        ]);

        // Password reset user
        User::create([
            'role_id' => Role::CLIENT_ROLE,
            'status' => User::STATUS_ACTIVE,
            'name' => "test_pwdreset_user",
            'email' => "test_pwdreset_user@payfasttest.com",
            'password' => md5('password'), // password
            'password_reset_key' => md5(now() . "test_pwdreset_user@payfasttest.com"),
            'password_reset_expires_at' => now()->addDays(2)
        ]);

        // Random Users
        $i = 0;
        while ($i < 500) {
            User::create([
                'role_id' => Role::CLIENT_ROLE,
                'status' => User::STATUS_ACTIVE,
                'name' => "randm_test_user_" . $i,
                'email' => "randm_test_user_" . $i . "@payfasttest.com",
                'password' => md5('password')
            ]);
            $i++;
        }
    }
}
