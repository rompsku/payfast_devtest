<?php

namespace Database\Seeders\Users;

use Illuminate\Database\Seeder;
use App\Models\{User, Role};

class AdminUserSeeder extends Seeder
{


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        User::create([
            'role_id' => Role::CLIENT_ROLE,
            'status' => User::STATUS_ACTIVE,
            'name' => "Admin",
            'email' => "romano.borman@gmail.com",
            'password' => md5('payfast!@#'), // password
        ]);
    }
}
