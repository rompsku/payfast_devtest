<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'users',
            function (Blueprint $table) {
                $table->bigIncrements("id");
                $table->unsignedBigInteger('role_id');
                $table->boolean('status')->default(0);
                $table->string('name');
                $table->string('email')->unique();
                $table->string('password');
                $table->string('password_reset_key')->nullable();
                $table->timestamp('password_reset_expires_at')->nullable();
                $table->timestamps();
                $table->softDeletes();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
