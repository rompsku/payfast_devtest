<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCinemasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cinemas', function (Blueprint $table) {
            $table->bigIncrements("id");
            $table->string('title');
            $table->boolean('status')->default(0);
            $table->string('geo_location')->nullable();
            $table->tinyText('address')->nullable();
            $table->text('description')->nullable();
            $table->string('image_thumb')->nullable();
            $table->string('image_large')->nullable();
            $table->string('image_interior')->nullable();
            $table->string('image_exterior')->nullable();
            $table->string('preview_clip')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cinemas');
    }
}
