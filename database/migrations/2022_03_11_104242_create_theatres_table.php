<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTheatresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('theatres', function (Blueprint $table) {
            $table->bigIncrements("id");
            $table->bigInteger("cinema_id");
            $table->boolean('status')->default(0);
            $table->string('title');
            $table->text('description')->nullable();
            $table->string('image_seating_grid')->nullable();
            $table->string('image_thumb')->nullable();
            $table->string('image_large')->nullable();
            $table->string('image_interior_1')->nullable();
            $table->string('image_interior_2')->nullable();
            $table->string('image_interior_3')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('theatres');
    }
}
