<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMoviesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movies', function (Blueprint $table) {
            $table->bigIncrements("id");
            $table->string('title');
            $table->boolean('status')->default(0);
            $table->string('age_rating')->nullable();
            $table->string('runtime')->nullable();
            $table->text('plot')->nullable();
            $table->string('genres')->nullable();
            $table->string('director')->nullable();
            $table->string('writer')->nullable();
            $table->tinyText('actors')->nullable();
            $table->tinyText('other_actors')->nullable();
            $table->string('languages')->nullable();
            $table->string('countries')->nullable();
            $table->string('awards')->nullable();
            $table->string('poster')->nullable();
            $table->string('trailer')->nullable();
            $table->decimal('imdb_rating')->nullable();
            $table->string('imdb_id')->nullable();
            $table->string('box_office')->nullable();
            $table->dateTime('release_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('movies');
    }
}
