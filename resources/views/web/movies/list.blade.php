@extends('web.layout.main-layout')

@section('page-content')
<section class="ftco-section ftco-no-pt ftco-no-pb">
    <div class="container px-md-0">
        <div class="row d-flex no-gutters">

            @foreach($movies as $movie)
            <div class="col-md-4 portfolio-wrap-2">
                <div class="row no-gutters align-items-center">
                    <div href="#" class="img w-100 js-fullheight d-flex align-items-center"
                        style="background-image: url({{$movie->poster}});">
                        <div class="text p-4 p-md-5 ftco-animate">
                            <div class="desc">
                                <div class="top">
                                    <span class="subheading">{{$movie->genres}}</span>
                                    <h2 class="mb-4"><a
                                            href="{{ config('app.url')}}/movies/{{$movie->id}}">{{$movie->title}}</a>
                                    </h2>

                                    <p class="mb-4">{{substr($movie->plot, 0, 150)}}
                                        <br/><br/>
                                        {{ $movie->age_rating }}, {{ $movie->runtime }}</p>
                                    <p><a href="{{ config('app.url')}}/movies/{{$movie->id}}" class="custom-btn">Read
                                            More</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach

        </div>
    </div>
</section>
@endsection