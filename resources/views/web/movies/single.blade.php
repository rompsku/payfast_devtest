@extends('web.layout.main-layout')

@section('page-content')
<section class="ftco-section ftco-no-pt ftco-no-pb">
    <div class="container px-md-0">
        <div class="row d-flex no-gutters">
            <div class="col-lg-12 pb-5 single">
                <div class="row">
                    <div class="img img-single w-100" style="background-image: url({{ $movie->poster }});"></div>

                    <div class="px-5 mt-4 page-content-block">
                        <h1 class="mb-3">{{ $movie->title }} <a href="{{ config('app.web_route_base')}}/bookings?m={{ $movie->id }}" class="custom-btn book-now-btn-movie"
                                id="bookingButton">BOOK NOW</a>
                        </h1>
                        <h4 class="mb-3">{{ $movie->actors }}</h4>
                        <p>{{ $movie->plot }}</p>
                        <div class="tag-widget post-tag-container mb-5 mt-5">
                            <div class="tagcloud">
                                @foreach(explode(",", $movie->genres) as $g)

                                <a href="#" class="tag-cloud-link">{{ trim($g) }}</a>
                                @endforeach
                            </div>
                        </div>
                        <p><strong>Rated: {{ $movie->age_rating }}</strong> | {{ $movie->runtime }} | {{
                            $movie->languages }} </p>
                        <p><strong>Director:</strong> {{ $movie->director }}</p>
                        <p><strong>Writer:</strong> {{ $movie->writer }}</p>
                    </div>
                </div><!-- END-->
            </div>
        </div>
    </div>
</section>
@endsection