@extends('web.layout.main-layout')

@section('page-content')
<section class="ftco-section ftco-no-pt ftco-no-pb">
    <div class="container px-md-0">
        <div class="row d-flex no-gutters">

            @foreach($cinemas as $cinema)
            <div class="col-md-12 portfolio-wrap">
                <div class="row no-gutters align-items-center">
                    <a href="{{$cinema[0]['image_exterior']}}"
                        class="col-md-6 img image-popup js-fullheight d-flex align-items-center justify-content-center"
                        style="background-image: url({{$cinema[0]['image_exterior']}});">
                        <div class="icon d-flex align-items-center justify-content-center">
                            <span class="fa fa-expand"></span>
                        </div>
                    </a>
                    <div class="col-md-6">
                        <div class="text pt-5 pl-0 px-lg-5 pl-md-4 ftco-animate">
                            <div class="px-4 px-lg-4">
                                <div class="desc">
                                    <div class="top">
                                        <span class="subheading">{{$cinema[0]['geo_location']}}</span>
                                        <h2 class="mb-4"><a href="{{ config('app.url')}}/cinemas/{{$cinema[0]['id']}}">{{$cinema[0]['title']}}</a></h2>
                                    </div>
                                    <div class="absolute">
                                        <p>{{$cinema[0]['description']}}

                                            <br /><br />
                                            <strong>{{$cinema[0]['address']}}</strong>
                                        </p>
                                    </div>
                                    <p><a href="{{ config('app.url')}}/cinemas/{{$cinema[0]['id']}}" class="custom-btn">Read More</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            @if($cinema[1])
            <div class="col-md-12 portfolio-wrap">
                <div class="row no-gutters align-items-center">
                    <a href="{{$cinema[1]['image_exterior']}}"
                        class="col-md-6 order-md-last img image-popup js-fullheight d-flex align-items-center justify-content-center"
                        style="background-image: url({{$cinema[1]['image_exterior']}});">
                        <div class="icon d-flex align-items-center justify-content-center">
                            <span class="fa fa-expand"></span>
                        </div>
                    </a>
                    <div class="col-md-6">
                        <div class="text pt-5 px-md-5 ftco-animate">
                            <div class="px-4 px-lg-4">
                                <div class="desc text-md-right">
                                    <div class="top">
                                        <span class="subheading">{{$cinema[1]['geo_location']}}</span>
                                        <h2 class="mb-4"><a href="{{ config('app.url')}}/cinemas/{{$cinema[1]['id']}}">{{$cinema[1]['title']}}</a></h2>
                                    </div>
                                    <div class="absolute">
                                        <p>{{$cinema[1]['description']}}</p>
                                    </div>
                                    <p><a href="{{ config('app.url')}}/cinemas/{{$cinema[1]['id']}}" class="custom-btn">Read More</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif
            @endforeach
        </div>
    </div>
</section>
@endsection