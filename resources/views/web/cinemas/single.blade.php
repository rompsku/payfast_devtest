@extends('web.layout.main-layout')

@section('page-content')
<section class="ftco-about img ftco-section ftco-no-pt ftco-no-pb" id="about-section">
    <div class="container-fluid px-0">
        <div class="row d-flex">
            <div class="col-md-6 d-flex">
                <div class="img d-flex align-self-stretch align-items-center js-fullheight"
                    style="background-image:url({{ $cinema->image_thumb}});">
                </div>
            </div>
            <div class="col-md-6 d-flex align-items-center">
                <div class="text px-4 pt-5 pt-md-0 px-md-4 pr-md-5 ftco-animate">
                    <h2 class="mb-4">{{ $cinema->title}}</h2>
                    <p>{{ $cinema->description}}</p>
                    <div class="team-wrap row mt-4">
                        <div class="col-md-4 team">
                            <div class="img" style="background-image: url({{ $cinema->image_large}});"></div>
                            <h3>4</h3>
                            <span>Theatres</span>
                        </div>
                        <div class="col-md-4 team">
                            <div class="img" style="background-image: url({{ $cinema->image_interior}});"></div>
                            <h3>435</h3>
                            <span>Total Seats</span>
                        </div>
                        <div class="col-md-4 team">
                            <div class="img" style="background-image: url({{ $cinema->image_exterior}});"></div>
                            <h3>21314</h3>
                            <span>Tickets Booked</span>
                        </div>
                    </div>
                    <div class="row">

                        <div class="col-12 text-center">
                            <a href="{{ config('app.web_route_base')}}/bookings?c={{ $cinema->id }}" class="custom-btn book-now-btn-cinema"
                                id="bookingButton">BOOK NOW</a>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection