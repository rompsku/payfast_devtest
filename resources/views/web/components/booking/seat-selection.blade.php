@section('seat-selection')
<div class="col-md-3 pricing">
    <div class="row no-gutters align-items-center">
        <div href="#" class="img w-100 js-fullheight d-flex align-items-center" id="bookingSeatBG">
            <div class="text p-4 ftco-animate">
                <h3>SEATS</h3>
                <p id="bookingSeatAmount"></p>
                <p><a href="#" class="btn-custom greyed-out" id="bookingSeatChangeButton"
                        onclick="launchSeatSelectionModal()">Select</a></p>
            </div>
        </div>
    </div>
</div>
@endsection