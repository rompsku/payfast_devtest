@section('movie-selection')
@if($movie)
<div class="col-md-3 pricing">
    <div class="row no-gutters align-items-center">
        <div href="#" class="img w-100 js-fullheight d-flex align-items-center" id="bookingMovieBG"
            style="background-image: url({{$movie->poster}});">
            <div class="text p-4 ftco-animate">
                
                <h3>MOVIE</h3>
                <h2 id="bookingMovieTitle">{{ $movie->title }}</h2>
                <ul>
                    <li><span>Rated: {{ $movie->age_rating }}</span></li>
                    <li><span>Runtime: {{ $movie->runtime }}</span></li>
                    <li><span>Language: {{ $movie->languages }}</span></li>
                </ul>
                <p><a href="#" class="btn-custom" id="bookingMovieChangeButton" onclick="launchMovieSelectionModal()">Change</a></p>
            </div>
        </div>
    </div>
</div>
@else
<div class="col-md-3 pricing">
    <div class="row no-gutters align-items-center">
        <div href="#" class="img w-100 js-fullheight d-flex align-items-center" id="bookingMovieBG">
            <div class="text p-4 ftco-animate">
                <h3>MOVIE</h3>
                <h2 id="bookingMovieTitle"></h2>
                <ul>
                    <li id="bookingMovieRating"></li>
                    <li id="bookingMovieRuntime"></li>
                    <li id="bookingMovieLanguage"></li>
                </ul>
                <p><a href="#" class="btn-custom" id="bookingMovieChangeButton" onclick="launchMovieSelectionModal()">Select</a></p>
            </div>
        </div>
    </div>
</div>
@endif
@endsection