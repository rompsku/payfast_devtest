@section('timeslot-selection')
<div class="col-md-3 pricing">
    <div class="row no-gutters align-items-center">
        <div href="#" class="img w-100 js-fullheight d-flex align-items-center"  id="bookingTimeslotBG"
        >
            <div class="text p-4 ftco-animate">
                <h3>TIME SLOT</h3>
                <p id="bookingTimeslotDateTime"></p>
                <p id="bookingTimeslotTheatre"></p>
                <p><a href="#" class="btn-custom greyed-out" id="bookingTimeslotChangeButton"  onclick="launchTimeslotSelectionModal()">Select</a></p>
            </div>
        </div>
    </div>
</div>
@endsection