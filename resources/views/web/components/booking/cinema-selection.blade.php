@section('cinema-selection')

<div class="col-md-3 pricing">
    <div class="row no-gutters align-items-center">
        @if($cinema)
        <div href="#" class="img w-100 js-fullheight d-flex align-items-center"
            style="background-image: url({{$cinema->image_exterior}});" id="bookingCinemaBG">
            <div class="text p-4 ftco-animate">
                <h3>CINEMA</h3>
                <h2 id="bookingCinemaTitle">{{ $cinema->title }}</h2>
                <ul>
                    <li id="bookingCinemaAddress"><span>Address:</span>{{ $cinema->address }}</li>
                </ul>
                <p><a href="#" class="btn-custom" id="bookingCinemaChangeButton" onclick="launchCinemaSelectionModal()">Change</a></p>
            </div>
        </div>
        @else
        <div href="#" class="img w-100 js-fullheight d-flex align-items-center" id="bookingCinemaBG">
            <div class="text p-4 ftco-animate">
                <h3>CINEMA</h3>
                <h2 id="bookingCinemaTitle"></h2>
                <ul>
                    <li id="bookingCinemaAddress"></li>
                </ul>
                <p><a href="#" class="btn-custom" id="bookingCinemaChangeButton" onclick="launchCinemaSelectionModal()">SELECT</a></p>
            </div>
        </div>
        @endif
    </div>
</div>
@endsection