@include('web.components.modal')
<!DOCTYPE html>
<html lang="en">

<head>
    <title>Cinema Cartel - Developer Demo</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Abril+Fatface&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ config('app.web_route_base')}}/css/animate.css">
    <link rel="stylesheet" href="{{ config('app.web_route_base')}}/css/owl.carousel.min.css">
    <link rel="stylesheet" href="{{ config('app.web_route_base')}}/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="{{ config('app.web_route_base')}}/css/magnific-popup.css">
    <link rel="stylesheet" href="{{ config('app.web_route_base')}}/css/flaticon.css">
    <link rel="stylesheet" href="{{ config('app.web_route_base')}}/css/style.css">
    <link rel="stylesheet" href="{{ config('app.web_route_base')}}/css/web/custom-styles.css">
</head>

<body>
    <div id="pfdemo-page">
        <a href="#" class="js-pfdemo-nav-toggle pfdemo-nav-toggle"><i></i></a>
        <aside id="pfdemo-aside" role="complementary" class="js-fullheight">

            <h1 id="pfdemo-logo" class="mb-4 mb-md-5"><a href="/"
                    style="background-image: url({{ config('app.web_route_base')}}/images/bg_1.jpg)">Cinema Cartel</a>
            </h1>
            <nav id="pfdemo-main-menu" role="navigation">
                <ul>
                    @if(request()->get('session')->user)
                    <li>Welcome back, {{ request()->get('session')->user->name }}</li>
                    <li>
                        <hr />
                    </li>
                    @endif
                    <li {!! ($menu_item=='home' ? 'class="pfdemo-active"' : '' ) !!}><a href="/">Home</a></li>
                    <li {!! ($menu_item=='movies' ? 'class="pfdemo-active"' : '' ) !!}><a href="/movies">Movies</a></li>
                    <li {!! ($menu_item=='cinemas' ? 'class="pfdemo-active"' : '' ) !!}><a href="/cinemas">Cinemas</a>
                    </li>
                    <li>
                        <hr />
                    </li>

                    @if(!request()->get('session')->user)
                    <li {!! ($menu_item=='login' ? 'class="pfdemo-active"' : '' ) !!}><a href="#"
                            onclick="openLoginBlock()" id="login-link">SIGN IN</a> | <a href="#"
                            onclick="openRegisterBlock()" id="register-link">SIGN UP</a>
                    </li>
                    @else
                    <li {!! ($menu_item=='my-bookings' ? 'class="pfdemo-active"' : '' ) !!}><a href="/my-bookings">My
                            Bookings</a>
                    </li>
                    <li><a href="#" onclick="logoutUser()">Logout</a>
                    </li>
                    @endif
                </ul>
            </nav>

            @if(!request()->get('session')->user)
            {{-- --}}

            <div class="login-block">
                <form method="POST" id="loginForm" name="loginForm" class="loginForm">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group">

                                <input type="email" class="form-control" name="email" id="login_email"
                                    placeholder="Email">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input type="password" class="form-control" name="password" id="login_password"
                                    placeholder="Password">
                            </div>
                        </div>
                        <div class="col-md-12 text-center">
                            <div class="form-group">
                                <span class="btn btn-primary" onclick="submitLoginForm()">SIGN IN</span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

            <div class="register-block">
                <form method="POST" id="registerForm" name="registerForm" class="registerForm">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">

                                <input type="text" class="form-control" name="name" id="register_name"
                                    placeholder="Name">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">

                                <input type="email" class="form-control" name="email" id="register_email"
                                    placeholder="Email">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <input type="password" class="form-control" name="password" id="register_password"
                                    placeholder="Password">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <input type="password" class="form-control" name="password_confirm"
                                    id="register_confirm_password" placeholder="Confirm Password">
                            </div>
                        </div>
                        <div class="col-md-12 text-center">
                            <div class="form-group">
                                <span class="btn btn-primary" onclick="submitRegistrationForm()">SIGN UP</span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            @endif


            <div id="auth-messages" class="text-center"></div>

            <div class="pfdemo-footer">
                <div class="mb-4">
                    <h3>Next movie starting soon...</h3>

                </div>
                <p class="pfooter">
                    <a href="{{ config('app.web_route_base')}}/movies/{{$next_showing->movie->id}}" target="_blank">{{
                        $next_showing->movie->title }}</a> @
                    <a href="{{ config('app.web_route_base')}}/cinemas/{{$next_showing->theatre->cinema->id}}"
                        target="_blank">{{ $next_showing->theatre->cinema->title }}</a>
                    <br />
                    {!! date("F j, Y, g:i a", strtotime($next_showing->starts_at)) !!}
                </p>
            </div>
        </aside> <!-- END pfdemo-ASIDE -->
        <div id="pfdemo-main">

            @yield('page-content')

        </div><!-- END pfdemo-MAIN -->
    </div><!-- END pfdemo-PAGE -->

    @yield('modal')

    <!-- loader -->
    <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px">
            <circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee" />
            <circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10"
                stroke="#F96D00" />
        </svg></div>
    <script src="{{ config('app.web_route_base')}}/js/jquery.min.js"></script>
    <script src="{{ config('app.web_route_base')}}/js/jquery-migrate-3.0.1.min.js"></script>
    <script src="{{ config('app.web_route_base')}}/js/popper.min.js"></script>
    <script src="{{ config('app.web_route_base')}}/js/bootstrap.min.js"></script>
    <script src="{{ config('app.web_route_base')}}/js/jquery.easing.1.3.js"></script>
    <script src="{{ config('app.web_route_base')}}/js/jquery.waypoints.min.js"></script>
    <script src="{{ config('app.web_route_base')}}/js/jquery.stellar.min.js"></script>
    <script src="{{ config('app.web_route_base')}}/js/owl.carousel.min.js"></script>
    <script src="{{ config('app.web_route_base')}}/js/jquery.magnific-popup.min.js"></script>
    <script src="{{ config('app.web_route_base')}}/js/jquery.animateNumber.min.js"></script>
    <script src="{{ config('app.web_route_base')}}/js/scrollax.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false">
    </script>
    <script src="{{ config('app.web_route_base')}}/js/google-map.js"></script>
    <script src="{{ config('app.web_route_base')}}/js/main.js"></script>
    <script src="{{ config('app.web_route_base')}}/js/web/app.js"></script>
    <script src="{{ config('app.web_route_base')}}/js/web/custom-scripts.js"></script>
    <script>
        var pf_cookie = getCookie("pf-api-key");
        var website_url = "{{ config('app.web_route') }}";
        var api_url = "{{ config('app.api_route') }}";
    </script>

</body>

</html>