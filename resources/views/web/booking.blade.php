@extends('web.layout.main-layout')
@include('web.components.booking/cinema-selection')
@include('web.components.booking/movie-selection')
@include('web.components.booking/timeslot-selection')
@include('web.components.booking/seat-selection')

@section('page-content')
<section class="ftco-section ftco-no-pt ftco-no-pb">
    @if(request()->get('session')->user_id)
    <div class="container px-md-0">
        <div class="row d-flex no-gutters">

        

            <form>
                <input type="hidden" name="cinema_id" id="booking_form_cinema_id" value="{{ ($cinema ? $cinema->id : '') }}">
                <input type="hidden" name="movie_id" id="booking_form_movie_id" value="{{ ($movie ? $movie->id : '') }}">
                <input type="hidden" name="timeslot_id" id="booking_form_time_slot_id">
                <input type="hidden" name="seats_count" id="booking_form_seat_ids">
            </form>

            @yield('cinema-selection')
            @yield('movie-selection')
            @yield('timeslot-selection')
            @yield('seat-selection')

        </div>
        <div class="row d-flex booking-complete-button">
            <div class="col-md-12">
                <a href="#" class="btn-custom-load d-block w-100 text-center py-4 greyed-out" id="bookingCompleteBtn" onclick="completeBooking()">Complete Booking <span class="fa fa-share"></span></a>
            </div>
        </div>
    </div>
    @else
    <div class="row d-flex no-gutters">
        <div class="col-12 portfolio-wrap-2">
            <div class="text p-4 p-md-5 ftco-animate fadeInUp ftco-animated">
                <h1>Membership Required</h1>
                <p>Please sign in, or sign up a new account, to continue with your booking.</p>
            </div>
        </div>
    </div>
    @endif

    
</section>
@endsection