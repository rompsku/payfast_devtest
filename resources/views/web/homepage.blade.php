@extends('web.layout.main-layout')

@section('page-content')
<section class="ftco-section ftco-no-pt ftco-no-pb">
    <div class="container px-md-0">
        <div class="row d-flex no-gutters">
            <div class="col-md-12 portfolio-wrap">
                <div class="row no-gutters align-items-center">
                    <a href="{{ $movie->poster }}"
                        class="col-md-6 img image-popup js-fullheight d-flex align-items-center justify-content-center"
                        style="background-image: url({{ $movie->poster }});">
                        <div class="icon d-flex align-items-center justify-content-center">
                            <span class="fa fa-expand"></span>
                        </div>
                    </a>
                    <div class="col-md-6">
                        <div class="text pt-5 pl-0 px-lg-5 pl-md-4 ftco-animate">
                            <div class="px-4 px-lg-4">
                                <div class="desc">
                                    <div class="top">
                                        <span class="subheading">Featured Movie</span>
                                        <h2 class="mb-4"><a
                                                href="{{ config('app.web_route_base')}}/movies/{{ $movie->id }}">{{
                                                $movie->title }}</a></h2>
                                    </div>
                                    <div class="absolute">
                                        <p>{{ substr($movie->plot, 0, 250) }}...
                                            <br /><br />
                                            {{ $movie->age_rating }}, {{ $movie->runtime }}
                                        </p>
                                    </div>
                                    <p><a href="{{ config('app.web_route_base')}}/movies/{{$movie->id}}"
                                            class="custom-btn">Read More</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12 portfolio-wrap">
                <div class="row no-gutters align-items-center">
                    <a href="{{ $cinema->image_thumb }}"
                        class="col-md-6 order-md-last img image-popup js-fullheight d-flex align-items-center justify-content-center"
                        style="background-image: url({{ $cinema->image_exterior }});">
                        <div class="icon d-flex align-items-center justify-content-center">
                            <span class="fa fa-expand"></span>
                        </div>
                    </a>
                    <div class="col-md-6">
                        <div class="text pt-5 px-md-5 ftco-animate">
                            <div class="px-4 px-lg-4">
                                <div class="desc text-md-right">
                                    <div class="top">
                                        <span class="subheading">Featured Cinema</span>
                                        <h2 class="mb-4"><a
                                                href="{{ config('app.web_route_base')}}/cinemas/{{ $cinema->id }}">{{
                                                $cinema->title }}</a></h2>
                                    </div>
                                    <div class="absolute">
                                        <p>{{ substr($cinema->description, 0, 250) }}...</p>
                                    </div>
                                    <p><a href="{{ config('app.web_route_base')}}/cinemas/{{ $cinema->id }}"
                                            class="custom-btn">View Details</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection