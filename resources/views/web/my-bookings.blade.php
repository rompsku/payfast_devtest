@extends('web.layout.main-layout')

@section('page-content')
<section class="ftco-section ftco-no-pt ftco-no-pb">
    <div class="container px-md-0">
        <div class="row d-flex no-gutters">


            @if(!$bookings->count())
            <div class="row d-flex no-gutters">
                <div class="col-12 portfolio-wrap-2">
                    <div class="text p-4 p-md-5 ftco-animate fadeInUp ftco-animated">
                        <h1>No bookings found...</h1>
                    </div>
                </div>
            </div>
            @else
            @foreach($bookings as $booking)
            <div class="col-md-4 portfolio-wrap-2" id="cancelBooking{{ $booking->id}}">

                <div class="row no-gutters align-items-center">
                    <div href="#" class="img w-100 js-fullheight d-flex align-items-center"
                        style="background-image: url({{ $booking->timeslot->movie->poster}}); height: 890px;">
                        <div class="text p-4 p-md-5 ftco-animate fadeInUp ftco-animated">
                            <div class="desc">
                                <div class="top">
                                    <p><small>REFERENCE CODE: {{ $booking->booking_reference}}</small></p>
                                    <span class="subheading">{{ $booking->timeslot->starts_at}}</span>
                                    <h2 class="mb-4">{{ $booking->timeslot->movie->title}}</h2>
                                    <p><strong>{{ $booking->timeslot->theatre->cinema->title}}</strong><br /><br />
                                        {{ $booking->timeslot->theatre->cinema->address}}</p>
                                    <h3>{{ $booking->tickets->count() }} SEATS</h3>
                                    <p><a href="#" class="custom-btn cancel-booking-btn"
                                            onclick="cancelBooking({{ $booking->id}})"
                                            id="cancelBookingBtn{{ $booking->id}}">Cancel Booking</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
            @endif
        </div>
    </div>
    @endsection