<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix(config("app.web_route_base"))->namespace('Web')->middleware('web', 'web.auth')->group(
    function () {

        // Public HTML home page
        Route::get('/', 'HomePageController@index');

        // Page for making bookings
        Route::get('/bookings', 'BookingController@create');

        // Page for making bookings
        Route::get('/my-bookings', 'BookingController@index');

        /**
         * Movies section
         */
        Route::prefix("movies")->group(
            function () {
                Route::get('/', 'MovieController@index');  // Comment goes here
                Route::get('/{id}', 'MovieController@show');  // Comment goes here
            }
        );

        /**
         * Cinemas section
         */
        Route::prefix("cinemas")->group(
            function () {
                Route::get('/', 'CinemaController@index');  // Comment goes here
                Route::get('/{id}', 'CinemaController@show');  // Comment goes here
            }
        );
    }
);
