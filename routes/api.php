<?php

use Illuminate\Support\Facades\Route;

// 
Route::prefix(config("app.api_route"))->namespace('API')->middleware('api')->group(
    function () {

        // Public HTML home page for API. Hide from bots!
        Route::get('/', 'HomePageController@index');

        /**
         * Web Routes
         * For frontend website related requests
         */
        Route::prefix("web")->middleware('web.auth')->group(
            function () {
                Route::get('cinemas', 'CinemaController@index'); // Comment goes here

                Route::get('movies', 'MovieController@index'); // Comment goes here

                Route::get('timeslots', 'TimeslotController@index'); // Comment goes here

                Route::get('seats', 'SeatController@index'); // Comment goes here

                Route::post('bookings', 'BookingController@store'); // Comment goes here
                Route::post('bookings/cancel', 'BookingController@delete'); // Comment goes here
            }
        );

        /**
         * Auth Routes
         * For login, password reset and general user authentication
         */
        Route::namespace('Auth')->prefix("auth")->group(
            function () {
                Route::post('login', 'AccessController@login'); // Comment goes here
                Route::get('logout', 'AccessController@logout'); // Comment goes here

                Route::post('register', 'RegisterController@register'); // Comment goes here
                Route::post('verify-email', 'RegisterController@verifyEmail'); // Comment goes here

                Route::post('password-reset', 'PasswordResetController@reset'); // Comment goes here
            }
        );

        /**
         * Admin Dashboard API Routes
         */
        Route::namespace('Admin')->prefix("admin")->middleware('api.auth')->group(
            function () {

                /**
                 * User Management Routes
                 */
                Route::namespace('User')->prefix("users")->group(
                    function () {
                        Route::get('/', 'CRUDController@index');  // Comment goes here
                        Route::post('/', 'CRUDController@create'); // Comment goes here

                        Route::get('/{id}', 'CRUDController@show');  // Comment goes here
                        Route::post('/{id}/update', 'CRUDController@update'); // Comment goes here
                        Route::post('/{id}/delete', 'CRUDController@delete'); // Comment goes here
                    }
                );

                /**
                 * Cinema Management Routes
                 */
                Route::namespace('Cinema')->prefix("cinemas")->group(
                    function () {
                        Route::get('/', 'CRUDController@index');  // Comment goes here
                        Route::post('/', 'CRUDController@create'); // Comment goes here

                        Route::get('/{id}', 'CRUDController@show');  // Comment goes here
                        Route::post('/{id}/update', 'CRUDController@update'); // Comment goes here
                        Route::post('/{id}/delete', 'CRUDController@delete'); // Comment goes here
                    }
                );

                /**
                 * Movie Management Routes
                 */
                Route::namespace('Movie')->prefix("movies")->group(
                    function () {
                        Route::get('/', 'CRUDController@index');  // Comment goes here
                        Route::post('/', 'CRUDController@create'); // Comment goes here

                        Route::get('/{id}', 'CRUDController@show');  // Comment goes here
                        Route::post('/{id}/update', 'CRUDController@update'); // Comment goes here
                        Route::post('/{id}/delete', 'CRUDController@delete'); // Comment goes here
                    }
                );
            }
        );
    }
);
